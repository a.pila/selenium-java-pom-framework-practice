package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterMethod;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class TestBase {

    private static Properties properties = new Properties();
    public static WebDriver driver;
    public static EventFiringWebDriver eventDriver;
    public static WebEventListener eventListener;

    public TestBase() {
        try {
            properties = new Properties();
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + "/" + "src/main/resources/configuration.properties");
            properties.load(ip);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initialization() {

        String browserName = properties.getProperty("browser");

        if (browserName.equals("CHROME")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver");
            driver = new ChromeDriver();
        } else if (browserName.equals("FIREFOX")) {
            System.setProperty("webdriver.gecko.driver", "src/main/resources/drivers/geckodriver");
            driver = new FirefoxDriver();
        }

        eventDriver = new EventFiringWebDriver(driver);

        // Create object of EventListerHandler to register it with EventFiringWebDriver
        eventListener = new WebEventListener();
        eventDriver.register(eventListener);
        driver = eventDriver;

        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAT, TimeUnit.SECONDS);
        driver.navigate().to(properties.getProperty("url"));
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }


}
