package tests;

import base.TestBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.SignInPage;

public class SignInTest extends TestBase {

    @Test(description = "This test uses Page Factory", enabled = false)
    public void signInTest() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver");

        WebDriver driver = new ChromeDriver();

        driver.navigate().to("https://gopro.com/en/ro/");

        SignInPage objAccount = PageFactory.initElements(driver, SignInPage.class);

        objAccount.assertTextPresence();
    }

    @Test(description = "Will work on this test")
    public void firstTestCase() {
        initialization();
    }
}
